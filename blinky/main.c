/**
 * Copyright (c) 2014 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

/** @file
 *
 * @defgroup blinky_example_main main.c
 * @{
 * @ingroup blinky_example
 * @brief Blinky Example Application main file.
 *
 * This file contains the source code for a sample application to blink LEDs.
 *
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "app_uart.h"
#include "app_error.h"
#include "app_twi.h"
#include "app_util_platform.h"
#include "nrf.h"
#include "bsp.h"
#include "nrf_uart.h"
#include "nrf_delay.h"
#include "boards.h"
#include "pca10028.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_drv_twi.h"
#include "tcs3472.h"

#define UART_TX_BUFF_SIZE 256
#define UART_RX_BUFF_SIZE 256

#define UART_HWFC APP_UART_FLOW_CONTROL_DISABLED


#define TWI_INSTANCE_ID     0

/* Buffer for samples read from sensor */
static uint8_t color_buf[8];

/* Indicates if operation on TWI has ended. */
static volatile bool m_xfer_done = false;

/* TWI instance. */
static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);


/**
 * @brief Function for handling data from temperature sensor.
 *
 * @param[in] temp          Temperature in Celsius degrees read from sensor.
 */
__STATIC_INLINE void data_handler(uint8_t color_byte[8])
{
    printf("Color 0: %d\t", color_byte[0]);
    printf("Color 1: %d\t", color_byte[1]);
    printf("Color 2: %d\t", color_byte[2]);
    printf("Color 3: %d\t", color_byte[3]);
    printf("Color 4: %d\t", color_byte[4]);
    printf("Color 5: %d\t", color_byte[5]);
    printf("Color 6: %d\t", color_byte[6]);
    printf("Color 7: %d\r\n", color_byte[7]);
}

/**
 * @brief TWI events handler.
 */
void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
    switch (p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
            if (p_event->xfer_desc.type == NRF_DRV_TWI_XFER_RX)
            {
                data_handler(color_buf);
            }
            m_xfer_done = true;
            break;
        default:
            break;
    }
}

/**
 * TCS3472 initialization
 */
void tcs3472_init()
{
    ret_code_t err_code;

    // enable sensor
    m_xfer_done = false;
    uint8_t tx_start[2] = {TCS3472_CMD_EN, TCS3472_PON};
    err_code = nrf_drv_twi_tx(&m_twi, TCS3472_ADDR, tx_start, sizeof(tx_start), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);

    // set integration time - Max / 256 cycles
    m_xfer_done = false;
    uint8_t tx_integ[2] = {TCS3472_CMD_ATIME, TCS3472_ATIME_MAX};
    err_code = nrf_drv_twi_tx(&m_twi, TCS3472_ADDR, tx_integ, sizeof(tx_integ), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);

    // set gain - Min / 1x
    m_xfer_done = false;
    uint8_t tx_gain[2] = {TCS3472_CMD_CONTROL, TCS3472_GAIN_1X};
    err_code = nrf_drv_twi_tx(&m_twi, TCS3472_ADDR, tx_gain, sizeof(tx_gain), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
}

/**
 * @brief TWI initialization.
 */
void twi_init (void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t twi_tcs3472_config = {
       .scl                = ARDUINO_SCL_PIN,
       .sda                = ARDUINO_SDA_PIN,
       .frequency          = NRF_TWI_FREQ_400K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = false
    };

    err_code = nrf_drv_twi_init(&m_twi, &twi_tcs3472_config, twi_handler, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi);
    tcs3472_init();
}

/**
 * Read from TCS3472
 */
void readSensor()
{
    ret_code_t err_code;

    // enable the RGBC
    m_xfer_done = false;
    uint8_t tx_cmd_en[2] = {TCS3472_CMD_EN, TCS3472_PON_AEN};
    err_code = nrf_drv_twi_tx(&m_twi, TCS3472_ADDR, tx_cmd_en, sizeof(tx_cmd_en), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);

    // receive all 8 8-bit color numbers
    m_xfer_done = false;
    uint8_t tx_cmd_clear[1] = {TCS3472_CMD_CLEAR};
    err_code = nrf_drv_twi_tx(&m_twi, TCS3472_ADDR, tx_cmd_clear, sizeof(tx_cmd_clear), true);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
    m_xfer_done = false;
    err_code = nrf_drv_twi_rx(&m_twi, TCS3472_ADDR, &color_buf[0], 8);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);

    //TODO clean up color buffer and save number in variable
}

// A simple error handler for uart if something goes wrong...
void uart_error_handle(app_uart_evt_t * p_event)
{
    if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_communication);
    }
    else if (p_event->evt_type == APP_UART_FIFO_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_code);
    }
}

void motorStep(int orange, int yellow, int pink, int blue, int delay)
{
    nrf_gpio_pin_write(23, orange);
    nrf_gpio_pin_write(24, yellow);
    nrf_gpio_pin_write(25, pink);
    nrf_gpio_pin_write(28, blue);
    nrf_delay_ms(delay);
}


int move_motor(int motor_pos, int motor_delay, int motor_dir)
{
    int orange_arr[8] = {0,0,0,0,0,1,1,1};
    int yellow_arr[8] = {0,0,0,1,1,1,0,0};
    int pink_arr[8] = {0,1,1,1,0,0,0,0};
    int blue_arr[8] = {1,1,0,0,0,0,0,1};

    motorStep(orange_arr[motor_pos],
              yellow_arr[motor_pos],
              pink_arr[motor_pos],
              blue_arr[motor_pos],
              motor_delay);
    if(motor_dir)
    {
        motor_pos += 1;
        if(motor_pos > 7)
            motor_pos = 0;
    }
    else
    {
        motor_pos -= 1;
        if(motor_pos < 0)
            motor_pos = 7;
    }
    return motor_pos;
}

void dev_board_init()
{
    nrf_gpio_cfg_output(23);
    nrf_gpio_cfg_output(24);
    nrf_gpio_cfg_output(25);
    nrf_gpio_cfg_output(28);

    nrf_gpio_cfg_input(29, NRF_GPIO_PIN_NOPULL);
}

void uart_init()
{
    uint32_t err_code;
    const app_uart_comm_params_t comm_params =
    {
        RX_PIN_NUMBER,
        TX_PIN_NUMBER,
        RTS_PIN_NUMBER,
        CTS_PIN_NUMBER,
        APP_UART_FLOW_CONTROL_ENABLED,
        false, // parity = none
        UART_BAUDRATE_BAUDRATE_Baud115200
    };
    // pass all the values to this function to initialize the UART module
    APP_UART_FIFO_INIT(&comm_params,
                       UART_RX_BUFF_SIZE,
                       UART_TX_BUFF_SIZE,
                       uart_error_handle,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);

    APP_ERROR_CHECK(err_code); // check if everything initialized correctly
}

/**
 * @brief Function for application main entry.
 */
int main(void) // MAIN LOOP
{
    int motor_delay = 1;
    int motor_pos = 0;
    int motor_dir = 1;

    uint8_t cr = '0'; // variable to hold the UART char data

    bsp_board_leds_init();
    uart_init();
    dev_board_init();
    twi_init();

    printf("Hello PC from nordic Device!!\r\n");
    while (true) // MAIN LOOP
    {

        app_uart_get(&cr);
        switch (cr) {
        case 't':
            if(motor_dir)
                motor_dir = 0;
            else
                motor_dir = 1;

            printf("Toggle Motor Direction\r\n");
            break;
        case 's':
            printf("Sensor\r\n");
            readSensor();
            break;
        case 'c':
            printf("Closing UART connection");
            app_uart_close();
            break;
        default:
            break;
        }
        cr = '0'; // Something other than the previous value of cr
        motor_pos = move_motor(motor_pos, motor_delay, motor_dir); // move the motor
    }
}

/**
 *@}
 **/
